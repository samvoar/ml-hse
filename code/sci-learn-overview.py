import numpy as np
import urllib.request

# url with dataset
url = "http://archive.ics.uci.edu/ml" \
      "/machine-learning-databases/pima-indians-diabetes" \
      "/pima-indians-diabetes.data"
# download the file
raw_data = urllib.request.urlopen(url)
# load the CSV file as a numpy matrix
dataset = np.loadtxt(raw_data, delimiter=",")
# separate the data from the target attributes
X = dataset[:,0:7]
y = dataset[:,8]


from sklearn import preprocessing

n_X = preprocessing.normalize(X)
# pprint(n_X[0:3])

s_X = preprocessing.scale(X)
# pprint(s_X[0:3])

from sklearn import metrics

from sklearn.ensemble import ExtraTreesClassifier
model = ExtraTreesClassifier()
model.fit(X, y)
# display the relative importance of each attribute
# print(model.feature_importances_)


from sklearn.feature_selection import RFE
from sklearn.linear_model import LogisticRegression
model = LogisticRegression()
# create the RFE model and select 3 attributes
rfe = RFE(model, 3)
rfe = rfe.fit(X, y)
# summarize the selection of the attributes
# print(rfe.support_)
# print(rfe.ranking_)

border = 700
Xl, Xt = X[:border], X[border:]
yl, yt = y[:border], y[border:]

models = {}

from sklearn.linear_model import LogisticRegression
models['regression'] = LogisticRegression()

from sklearn.naive_bayes import GaussianNB
models['bayes'] = GaussianNB()

from sklearn.neighbors import KNeighborsClassifier
# fit a k-nearest neighbor model to the data
models['knn'] = KNeighborsClassifier()

from sklearn.tree import DecisionTreeClassifier
# fit a CART model to the data
models['tree'] = DecisionTreeClassifier()

from sklearn.svm import SVC
# fit a SVM model to the data
models['svm'] = SVC()

for algorithm, model in models.items():
    model.fit(Xl,yl)
    expected = yt
    predicted = model.predict(Xt)
    stats = metrics.precision_recall_fscore_support(expected, predicted)
    print(algorithm)
    print(metrics.classification_report(expected, predicted))
